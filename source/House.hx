package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.util.FlxColor;

/**
 * ...
 * @author 
 */
class House extends FlxSprite
{
	public var regainIdentityBox : FlxSprite;
	public var collide : FlxSprite;
	
	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		//this.makeGraphic(86*6, 86*5, FlxColor.BROWN);
		this.loadGraphic(AssetPaths.house__png, false, 688, 430);
		
		regainIdentityBox = new FlxSprite(X + 86, Y + 86);
		regainIdentityBox.makeGraphic (86 * 6, 86 * 3);
		
		collide = new FlxSprite(X, Y);
		collide.makeGraphic(Std.int(this.width), 86 * 2, FlxColor.RED);
		collide.immovable = true;
	}
	
	public override function update (elapsed)
	{
		super.update(elapsed);
		regainIdentityBox.update(elapsed);
	}
	
	public override function draw ()
	{
		super.draw();
		//regainIdentityBox.draw();
		//collide.draw();
	}
	
	
}