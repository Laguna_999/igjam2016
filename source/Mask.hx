package;

import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.text.FlxText;
import flixel.util.FlxColor;

/**
 * ...
 * @author 
 */
class Mask extends FlxSprite
{

	public var hasMask : Bool;
	public var isMaskActive: Bool;
	
	public var isHead : Bool;
	public var isFace : Bool;
	
	public var emptySlot: FlxSprite;
	public var activeImage : FlxSprite;
	
	public var text: FlxText;
	
	public var energydrainfactor : Float = 1;

	public function new(maskuiimagepath:Dynamic, maskuiimageactivepath:Dynamic, ID : Int, edf : Float = 1) 
	{
		super();
		energydrainfactor = edf;
		//this.makeGraphic(40, 40);
		this.loadGraphic(maskuiimagepath, false, 128, 128);
		this.scrollFactor.set();
		this.scale.set(0.75, 0.75);
		this.setPosition(150 + (0.75) * 158 * (ID - 1), -15);
		
		activeImage = new FlxSprite(x, y);
		activeImage.scale.set(0.75, 0.75);
		activeImage.loadGraphic(maskuiimageactivepath, false, 128, 128);
		activeImage.scrollFactor.set();
		
		emptySlot = new FlxSprite();
		emptySlot.scale.set(0.75, 0.75);
		emptySlot.loadGraphic(AssetPaths.UI_mask_empty__png, false, 128, 128);
		emptySlot.setPosition(x, y);
		emptySlot.scrollFactor.set();
		
		text = new FlxText(x+ 5 , y +24 , 64, Std.string(ID), 14);
		text.color = FlxColor.BLACK;
		text.scrollFactor.set();
	}
	
	
	public override function draw()
	{
		if ( hasMask)
		{
			if (isMaskActive)
			{
				activeImage.draw();
			}
			else
			{
				super.draw();
			}
			text.draw();
		}
		else
		{
			this.emptySlot.draw();
		}
	}
}