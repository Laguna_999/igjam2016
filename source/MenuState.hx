package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;
import flash.system.System; 

class MenuState extends FlxState
{
	private var background : FlxSprite;
	
	private var title : FlxText;
	
	private var it_play : FlxText;
	private var it_credits : FlxText;
	private var it_quit : FlxText;
	
	private var idx : Int = 0;
	
	override public function create():Void
	{
		super.create();
		FlxG.mouse.visible = false;
	
		FlxG.sound.playMusic(AssetPaths.music__ogg, 0.75, true);
		
		background = new FlxSprite();
		background.scrollFactor.set();
		background.makeGraphic(FlxG.width, FlxG.height, FlxColor.fromRGB(230, 230, 230));
		add(background);

		title = new FlxText(0, 0, 0, "Koza  i  Babushka", 64);
		title.setFormat(AssetPaths.WereWolf__ttf, 64, FlxColor.BLACK);
		title.alignment = flixel.text.FlxTextAlign.CENTER;
		title.screenCenter(FlxAxes.X);
		title.y = FlxG.height / 3;
		title.scrollFactor.set();
		add(title);
		
		it_play = new FlxText(0, 0, 0, "Play", 45);
		it_play.setFormat(AssetPaths.WereWolf__ttf, 45, FlxColor.BLACK);
		it_play.screenCenter(FlxAxes.X);
		it_play.y = FlxG.height / 3 * 2 - 100;
		
		it_credits = new FlxText(0, 0, 0, "Credits", 45);
		it_credits.setFormat(AssetPaths.WereWolf__ttf, 45, FlxColor.BLACK);
		it_credits.screenCenter(FlxAxes.X);
		it_credits.y = FlxG.height / 3 * 2 + 64 - 100;
		
		it_quit = new FlxText(0, 0, 0, "Quit", 45);
		it_quit.setFormat(AssetPaths.WereWolf__ttf, 45, FlxColor.BLACK);
		it_quit.screenCenter(FlxAxes.X);
		it_quit.y = FlxG.height / 3 * 2 + 64 * 2 - 100;
		
		add(it_play);
		add(it_credits);
		add(it_quit);
		
		
	}

	override public function update(elapsed:Float):Void
	{
		
		super.update(elapsed);
		//if (FlxG.keys.pressed.C)
			//FlxG.switchState(new CreditsState());
			//
		//else if (FlxG.keys.pressed.SPACE)
		//{
			//FlxG.switchState(new PlayState());
		//}
		
		if (FlxG.keys.justPressed.DOWN)
		{
			idx++;
			if (idx >= 3)
			{
				idx = 0;
			}
			
		}
		if (FlxG.keys.justPressed.UP)
		{
			idx--;
			if (idx < 0)
			{
				idx = 2;
			}
		}
		
		if (idx == 0)
		{
			FlxTween.tween(it_play, { y:FlxG.height / 3 * 2 +     0*64 - 100 }, 0.25);
			FlxTween.tween(it_credits, { y:FlxG.height / 3 * 2 +  1*64 - 100}, 0.25);
			FlxTween.tween(it_quit, { y:FlxG.height / 3 * 2 +     2*64 -100 }, 0.25);
			it_play.alpha = 1;
			it_credits.alpha = 0.5;
			it_quit.alpha = 0.5;
			if (FlxG.keys.justPressed.ENTER ||FlxG.keys.justPressed.SPACE)
			{
				
			FlxG.switchState(new PlayState());
				
				
			}
		}
		else if (idx == 1)
		{
			FlxTween.tween(it_play, { y:FlxG.height / 3 * 2 +   2*64 -100}, 0.25);
			FlxTween.tween(it_credits, { y:FlxG.height / 3 *2 + 0*64 -100}, 0.25);
			FlxTween.tween(it_quit, { y:FlxG.height / 3 * 2 +   1*64 -100 }, 0.25);
			it_play.alpha = 0.5;
			it_credits.alpha = 1;
			it_quit.alpha = 0.5;
			if (FlxG.keys.justPressed.ENTER||FlxG.keys.justPressed.SPACE)
			{

				FlxG.switchState(new CreditsState());
			}
		}
		else if (idx == 2)
		{
			FlxTween.tween(it_play, { y:FlxG.height / 3 * 2 +    1*64 -100}, 0.25);
			FlxTween.tween(it_credits, { y:FlxG.height / 3 * 2 + 2*64 -100}, 0.25);
			FlxTween.tween(it_quit, { y:FlxG.height / 3 * 2 +    0*64 -100 }, 0.25);
			it_play.alpha = 0.5;
			it_credits.alpha = 0.5;
			it_quit.alpha = 1.0;
			if (FlxG.keys.justPressed.ENTER||FlxG.keys.justPressed.SPACE)
			{
				System.exit(0);
			}
		}
	}
	
	
}
