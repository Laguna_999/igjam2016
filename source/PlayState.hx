package;

import flixel.FlxBasic;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.editors.ogmo.FlxOgmoLoader;
import flixel.group.FlxSpriteGroup;
import flixel.system.FlxSound;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.tweens.FlxTween;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxColor;
import flixel.util.FlxTimer;

class PlayState extends FlxState
{
	var player : Player;
	
	var map : FlxOgmoLoader;
	var level : FlxTilemap;
	var backgroundLayer : FlxTilemap;
	var foreground : FlxTilemap;
	
	var house : House;
	
	public var overlay : FlxSprite;
	private var ending : Bool;
	private var laddergroup : FlxSpriteGroup;
	private var laddercollidegroup : FlxSpriteGroup;
	
	private var hurtinggroup : FlxSpriteGroup;
	
	
	
	private var destroyablesgroup : FlxSpriteGroup;
	
	private var pickup_maskdeer : FlxSprite;
	private var pickup_maskbear : FlxSprite;
	private var pickup_maskowl : FlxSprite;
	private var pickup_masklynx1 : FlxSprite;
	private var pickup_masklynx2 : FlxSprite;

	private var deerBubble : FlxSprite;
	private var bearBubble : FlxSprite;
	private var owlBubble : FlxSprite;
	private var lynxBubble: FlxSprite;

	private var squirrelBubble: FlxSprite;
	private var goatBubble: FlxSprite;

	private var goatAssAnim: FlxSprite;
	private var pickup_goat: FlxSprite;

	
	private var door : FlxSprite;
	private var doorTriggerArea : FlxSprite;
	
	
	
	
	private var blackScreenOnDark : FlxSprite;
	private var lynxVision : FlxSprite;
	
	private var well : FlxSprite;
	
	private var backgroundTrees1 : FlxSpriteGroup;
	private var backgroundTrees2 : FlxSpriteGroup;
	
	private var background2 : FlxSprite;
	
	
	/////////////////////////////
	// Sound stuff
	/////////////////////////////
	
	var soundPickup_Deer : FlxSound;
	var soundPickup_Bear : FlxSound;
	var soundPickup_Owl :  FlxSound;
	var soundPickup_Lynx : FlxSound;
	var sound_atmo : FlxSound;
	var sound_atmo_thunder : FlxSound;
	var sound_bearhit : FlxSound;
	var bearhit_timer : Float = 0;
	
	
	var grandmaright : FlxSprite;
	
	override public function create():Void
	{
		FlxG.camera.bgColor = FlxColor.fromRGB(255, 255, 255);
		super.create();
		

		CreateBackground();

		
		player = new Player(4042 + 86 * 8, 13502 + 86 * 4.5, this);
		
		player.canInput = player.canMove = true;
		FlxG.camera.follow(player);
		
		map = new FlxOgmoLoader(AssetPaths.level__oel);
		level = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "NewLayer0");
		level.follow();
		level.setTileProperties(1, FlxObject.NONE);
		level.setTileProperties(2, FlxObject.ANY);
		
		
		backgroundLayer = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "Background");
		add(backgroundLayer);
		add(level);
		//trace( backgroundLayer.getTile(125, 159));
		
		
		///////////////////////////
		// FOREGROUND
		///////////////////////////
		foreground = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "Foreground");
		
		well = new FlxSprite(75 * 86 , 162 * 86);
		well.loadGraphic(AssetPaths.well__png, false, 258, 153);
		add(well);

		//Bubbles
		var animIDs = [for (i in 0...16) i];



		goatAssAnim = new FlxSprite();
		goatAssAnim.loadGraphic(AssetPaths.Goat_Goat_runaway__png, true, 130, 112);
		goatAssAnim.animation.add("ass", animIDs, 16, true);
		goatAssAnim.x = player.x + (86*5);
		goatAssAnim.y = player.y + 30;
		goatAssAnim.scale.set(-1,1);

		pickup_goat = new FlxSprite();
		pickup_goat.loadGraphic(AssetPaths.Goat_Goat_headshaking__png, true, 137, 151);
		pickup_goat.animation.add("head", animIDs, 16, true);
		pickup_goat.animation.play("head");
		pickup_goat.x = 138 * 86;
		pickup_goat.y = 45.2 * 86;
		pickup_goat.scale.set(-1,1);


		bearBubble = new FlxSprite();
		bearBubble.loadGraphic(AssetPaths.Think_Bubbles_bear__png, true, 185, 156);
		bearBubble.animation.add("bubble", animIDs, 4, false);
		bearBubble.alpha = 0;


		deerBubble = new FlxSprite();
		deerBubble.loadGraphic(AssetPaths.Think_Bubbles_deer__png, true, 185, 156);
		deerBubble.animation.add("bubble", animIDs, 4, false);
		deerBubble.alpha = 0;


		lynxBubble = new FlxSprite();
		lynxBubble.loadGraphic(AssetPaths.Think_Bubbles_lynx__png, true, 185, 156);
		lynxBubble.animation.add("bubble", animIDs, 4, false);
		lynxBubble.alpha = 0;


		owlBubble = new FlxSprite();
		owlBubble.loadGraphic(AssetPaths.Think_Bubbles_owl__png, true, 185, 156);
		owlBubble.animation.add("bubble", animIDs, 4, false);
		owlBubble.alpha = 0;


		squirrelBubble = new FlxSprite();
		squirrelBubble.loadGraphic(AssetPaths.Think_Bubbles_squirrel__png, true, 185, 156);
		squirrelBubble.animation.add("bubble", animIDs, 4, false);
		squirrelBubble.alpha = 0;


		goatBubble = new FlxSprite();
		goatBubble.loadGraphic(AssetPaths.Think_Bubbles_Goat__png, true, 185, 156);
		goatBubble.animation.add("bubble", animIDs, 4, false);
		goatBubble.alpha = 0;
		goatBubble.x = player.x;
		goatBubble.y = player.y - 140;

		
		///////////////////////////
		// ladder stuff 
		///////////////////////////
		var ladders : FlxTilemap;
		ladders = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "Ladders");
		
	
		laddergroup = new FlxSpriteGroup();
		laddercollidegroup = new FlxSpriteGroup();
		for (x in 0...ladders.widthInTiles)
		{
		for (y in 0...ladders.heightInTiles)
		{
			if (ladders.getTile(x, y) == 0) 
			{
				continue;
			}
			else
			{
				var l : FlxSprite  = new FlxSprite(x * 86, y * 86);
				//l.makeGraphic(86, 86, FlxColor.ORANGE);
				l.loadGraphic(AssetPaths.ladder__png, false, 86, 86);
				laddergroup.add(l);
				
				l = new FlxSprite(x * 86 + 36, (y-0.5) * 86 );
				l.makeGraphic(12, 86, FlxColor.WHITE);
				laddercollidegroup.add(l);
			}
		}
		}
		add (laddergroup);
		
		
		///////////////////////////
		// hurting stuff
		///////////////////////////
		
		
		
		var hurtings : FlxTilemap;
		hurtings = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "Hurting");
		//trace (hurtings.getTile(103, 165));
		
		hurtinggroup = new FlxSpriteGroup();
			for (x in 0...ladders.widthInTiles)
		{
		for (y in 0...hurtings.heightInTiles)
		{	
			var tileID : Int = hurtings.getTile(x, y);
			if (tileID == 0) 
			{
				continue;
			}
			else
			{
				if (tileID == 565)
				{
					
					var l : FlxSprite  = new FlxSprite(x * 86 , y * 86);
					l.loadGraphic(AssetPaths.water__png, false, 86, 86);
					hurtinggroup.add(l);
				}
				else
				{
					var l : FlxSprite  = new FlxSprite(x * 86 , y * 86 + 47);
					l.loadGraphic(AssetPaths.dangerousspikes__png, false, 86, 39);
					hurtinggroup.add(l);
				}
				
			}
		}
		}
		add (hurtinggroup);
		
		
		///////////////////////
		// Destroyables 
		///////////////////////
		var destroyables : FlxTilemap;
		destroyables = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "Destroyable");
		destroyablesgroup = new FlxSpriteGroup();
		for (x in 0...destroyables.widthInTiles)
		{
		for (y in 0...destroyables.heightInTiles)
		{	
			var tileID : Int = destroyables.getTile(x, y);
			if (tileID == 0) 
			{
				continue;
			}
			else
			{
				var l : FlxSprite  = new FlxSprite(x * 86 , y * 86);
				l.immovable = true;
				l.loadGraphic(AssetPaths.destroyable__png, false, 86, 86);
				destroyablesgroup.add(l);
			}
		}
		}
			add (destroyablesgroup);

		///////////////////////
		// Door Stuff 
		///////////////////////
		
		
		var doors : FlxTilemap;
		doors = map.loadTilemap(AssetPaths.terrainatlas__png, 86, 86, "Door_Hit_Right");
		for (x in 0...doors.widthInTiles)
		{
		for (y in 0...doors.heightInTiles)
		{	
			var tileID : Int = doors.getTile(x, y);
			if (tileID == 0) 
			{
				continue;
			}
			else
			{
				//trace("door loaded at " + Std.string(x) + "," + Std.string(y));
				door  = new FlxSprite(x * 86 , y * 86);
				
				
				door.immovable = true;
				door.loadGraphic(AssetPaths.door__png, false, 86, 258);
				
				doorTriggerArea = new FlxSprite((x + 1) * 86 , y * 86);
				doorTriggerArea.makeGraphic(86, 258, FlxColor.RED);
				
			}
		}
		}
		add (door);
		
		add(foreground);
		
		////////////////////
		// mask pickups
		////////////////////
		
		pickup_maskdeer = new FlxSprite(3526 - 1*86 , 14964);
		pickup_maskdeer.loadGraphic(AssetPaths.DeadDeer__png, false, 256, 125);
		add(pickup_maskdeer);
		
		pickup_maskbear = new FlxSprite(88*86, 170*86 - 1*86 + 10);
		pickup_maskbear.loadGraphic(AssetPaths.DeadBear__png, 350, 196);
		add(pickup_maskbear);
		
		pickup_maskowl  = new FlxSprite(18*86, 179*86 - 86 +43);
		pickup_maskowl.loadGraphic(AssetPaths.DeadOwl__png, false, 256, 182);
		add(pickup_maskowl);
		pickup_masklynx1 = new FlxSprite(43*86, 145*86+86);
		pickup_masklynx2 = new FlxSprite(43*86, 145*86+86);
		pickup_masklynx1.loadGraphic(AssetPaths.DeadLynxFront__png, false, 192, 120);
		pickup_masklynx2.loadGraphic(AssetPaths.DeadLynxBack__png, false, 192, 120);
		add(pickup_masklynx1);
		add(pickup_masklynx2);
		
		//pickup_goat = new FlxSprite(138 * 86, 45 * 86);
		//pickup_goat.makeGraphic(86 * 2, 86 * 2, FlxColor.BLUE);
		add(pickup_goat);
		
		
	
		add(player);
		player.sprite.alpha = 0;
		grandmaright = new FlxSprite(player.x, player.y);
		grandmaright.loadGraphic(AssetPaths.playerAnim__png, true, 86, 140);
		grandmaright.setPosition(player.x - 86 / 7, player.y - 20);
		grandmaright.animation.add("idle", [0]);
		grandmaright.scale.set( -1, 1);
		
		add(grandmaright);
		
		
		house = new House(4042, 13502+2*86);
		add(house);

		// Adding Bubbles
		add(bearBubble);
		add(deerBubble);
		add(lynxBubble);
		add(owlBubble);
		add(squirrelBubble);
		add(goatBubble);
		
		/////////////////////////////
		// Sound stuff
		/////////////////////////////
		
		soundPickup_Deer = FlxG.sound.load(AssetPaths.T13_mask_pickup_deer__ogg, 1);
		soundPickup_Bear = FlxG.sound.load(AssetPaths.T13_mask_pickup_bear__ogg, 1);
		soundPickup_Owl = FlxG.sound.load(AssetPaths.T13_mask_pickup_bird__ogg, 1);
		soundPickup_Lynx = FlxG.sound.load(AssetPaths.T13_mask_pickup_lynx__ogg, 1);
		sound_bearhit = FlxG.sound.load(AssetPaths.T13_BearHit_growl_02__ogg, 1);
		sound_atmo = FlxG.sound.load(AssetPaths.ForestAmbience_Filtered__ogg, 0.25, true );
		sound_atmo_thunder = FlxG.sound.load(AssetPaths.RumbleThunder_01__ogg, 0.25, true);
		sound_atmo_thunder.volume = 0;
		sound_atmo_thunder.play();
		sound_atmo.play();
	
		blackScreenOnDark = new FlxSprite (0, 0);
		blackScreenOnDark.makeGraphic(FlxG.width, FlxG.height, FlxColor.BLACK);
		blackScreenOnDark.origin.set();
		blackScreenOnDark.scrollFactor.set();
		blackScreenOnDark.alpha = 0;
		
		lynxVision = new FlxSprite(0, 0);
		lynxVision.loadGraphic(AssetPaths.lynxvision__png, false, 1024, 800);
		lynxVision.origin.set();
		lynxVision.scrollFactor.set();
		lynxVision.alpha = 0;
		
		overlay = new FlxSprite();
		overlay.makeGraphic(FlxG.width, FlxG.height, FlxColor.BLACK);
		overlay.alpha = 1.0;
		FlxTween.tween(overlay, { alpha:0 }, 0.75);
		overlay.scrollFactor.set();
		ending = false;

		add(goatAssAnim);

		gameStart();
	}
	
	function CreateBackground() 
	{
		backgroundTrees1 = new FlxSpriteGroup();
		backgroundTrees2 = new FlxSpriteGroup();
		var x : Float = 0;
		
		var sf : Float = 0.75;
		
		var my_y : Float = 11180 - 86*6;
		
		while (x <= 10062/sf)
		{
			x += GameProperties.rng.floatNormal(300, 50);
			
			var s: FlxSprite = new FlxSprite(x, my_y);
			
			var t : Int = GameProperties.rng.int(0, 2);
			if (t == 0)
			{
				s.loadGraphic(AssetPaths.tree1__png, false, 264, 4000);
			}
			else if (t == 1)
			{
				s.loadGraphic(AssetPaths.tree2__png, false, 264, 4000);
			}
			else
			{
				s.loadGraphic(AssetPaths.tree3__png, false, 264, 4000);
			}
			backgroundTrees1.add(s);
			s.scrollFactor.set(sf, 1);
			s.alpha = 0.7;
		}
		x = 0;
		while (x <= 10062/sf)
		{
			x += GameProperties.rng.floatNormal(300, 50);
			
			var s: FlxSprite = new FlxSprite(x, my_y);
			
			var t : Int = GameProperties.rng.int(0, 2);
			if (t == 0)
			{
				s.loadGraphic(AssetPaths.tree1__png, false, 264, 4000);
			}
			else if (t == 1)
			{
				s.loadGraphic(AssetPaths.tree2__png, false, 264, 4000);
			}
			else
			{
				s.loadGraphic(AssetPaths.tree3__png, false, 264, 4000);
			}
			backgroundTrees1.add(s);
			s.scrollFactor.set(sf, 1);
			s.alpha = 0.7;
		}
		backgroundTrees1.scrollFactor.set(sf, 1);
		add(backgroundTrees1);
		
		
		sf = 0.8;
		x = 0;
		
		while (x <= 10062/sf)
		{
			x += GameProperties.rng.floatNormal(300/sf, 50);
			
			var s: FlxSprite = new FlxSprite(x, my_y);
			
			var t : Int = GameProperties.rng.int(0, 2);
			if (t == 0)
			{
				s.loadGraphic(AssetPaths.tree1__png, false, 264, 4000);
			}
			else if (t == 1)
			{
				s.loadGraphic(AssetPaths.tree2__png, false, 264, 4000);
			}
			else
			{
				s.loadGraphic(AssetPaths.tree3__png, false, 264, 4000);
			}
			backgroundTrees2.add(s);
			s.scrollFactor.set(sf, 1);

		}
		x = 0;
		while (x <= 10062/sf)
		{
			x += GameProperties.rng.floatNormal(300/sf, 50);
			
			var s: FlxSprite = new FlxSprite(x, my_y);
			
			var t : Int = GameProperties.rng.int(0, 2);
			if (t == 0)
			{
				s.loadGraphic(AssetPaths.tree1__png, false, 264, 4000);
			}
			else if (t == 1)
			{
				s.loadGraphic(AssetPaths.tree2__png, false, 264, 4000);
			}
			else
			{
				s.loadGraphic(AssetPaths.tree3__png, false, 264, 4000);
			}
			backgroundTrees2.add(s);
			s.scrollFactor.set(sf, 1);

		}
		
		backgroundTrees2.scrollFactor.set(sf, 1);
		add(backgroundTrees2);
		
		background2 = new FlxSprite(108*86, 36*86);
		background2.makeGraphic(2, 2);
		background2.scale.set(10000, 10000);
		add(background2);
		
	}

	override public function update(elapsed:Float):Void
	{
		MyInput.update();
		overlay.update(elapsed);
		laddercollidegroup.update(elapsed);
		if (!ending)
		{
			super.update(elapsed);

			bearhit_timer -= elapsed;
			
			player.touchedGroundLast = player.touchedGround;
			FlxG.collide(player, level);
			FlxG.collide(player, house.collide);
			
			player.touchedGround = player.isTouching(FlxObject.DOWN);
			
			player.isonLadder = FlxG.overlap(player, laddercollidegroup);
			
			
			if (player.x > 121 * 86)	// player on the right side
			{
				if (player.y < 141*86)
				{
					sound_atmo_thunder.volume = 0.25;
				}
				
				if (player.y < 113*86)
				{
					sound_atmo_thunder.volume = 0.5;
				}
			}
			
			
			
			
			FlxG.overlap(player, destroyablesgroup, function (p : FlxSprite, d : FlxSprite)
			{
				if (d.alive)
				{

					if (player.isStrong) 
					{
						d.alive = false;
						d.alpha = 0;
						if (bearhit_timer <= 0)
						{
							FlxG.camera.shake(0.005, 0.125);
							sound_bearhit.play();
							bearhit_timer = 0.5;
						}
					}
					else
					{
						FlxObject.separate(p, d);
						
						// TODO if player has not yet found his mask, show him the bear sign
					}
				}
				

				
			});
			
			FlxG.overlap(player, door, function (p : FlxSprite, d : FlxSprite)
			{
				if (d.alive)
				{
					FlxObject.separate(p, d);
				}
				else
				{
					// TODO show door Intro bubble
				}
				
				
			});
			if (FlxG.overlap(player, doorTriggerArea))
			{
				var target : Float = door.y - 86 * 3;
				
				FlxTween.tween(door, { y: target }, 1, { onComplete :function (t:FlxTween) { door.alpha = 0; door.alive = false; }} );
				
				
				//door.alpha = 0;
			}
			
			
			checkDarkOverlay();	
			
			
			DoMaskPickups();
			
			
			
			
			if (FlxG.overlap(player, house.regainIdentityBox))
			{
				player.refillIdentity();
			}
			
			if (FlxG.overlap(player, hurtinggroup))
			{
				player.TakeDamage();
			}
			
			if (player.identityBar.health <= 0)
			{
				EndGame();
			}
		}	
	}
	
	function DoMaskPickups() 
	{
		if (FlxG.overlap(player, pickup_maskbear))
		{
			player.BearMask.hasMask = true;
			soundPickup_Bear.play();

			if(bearBubble.alive){
				bearBubble.x = player.x;
				bearBubble.y = player.y - 140;

				bearBubble.alpha = 1;
				bearBubble.animation.play("bubble");

				var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
				t.start(4.0,function(t:flixel.util.FlxTimer){
					bearBubble.alpha = 0;
					bearBubble.alive = false;
				});
			}

			pickup_maskbear.color = FlxColor.fromRGB(100,100,100);

		}
		if (FlxG.overlap(player, pickup_maskdeer))
		{
			player.DeerMask.hasMask = true;
			soundPickup_Deer.play();

			if(deerBubble.alive){
				deerBubble.x = player.x;
				deerBubble.y = player.y - 140;

				deerBubble.alpha = 1;
				deerBubble.animation.play("bubble");

				var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
				t.start(4.0,function(t:flixel.util.FlxTimer){
					deerBubble.alpha = 0;
					deerBubble.alive = false;
				});
			}

			pickup_maskdeer.color = FlxColor.fromRGB(100,100,100);

		}
		if (FlxG.overlap(player, pickup_masklynx1) || FlxG.overlap(player, pickup_masklynx2))
		{
			player.LynxMask.hasMask = true;
			soundPickup_Lynx.play();

			if(lynxBubble.alive){

				lynxBubble.x = player.x;
				lynxBubble.y = player.y - 140;

				lynxBubble.alpha = 1;
				lynxBubble.animation.play("bubble");

				var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
				t.start(4.0,function(t:flixel.util.FlxTimer){
					lynxBubble.alpha = 0;
					lynxBubble.alive = false;
				});
			}

			pickup_masklynx1.color = FlxColor.fromRGB(100,100,100);
			pickup_masklynx2.color = FlxColor.fromRGB(100,100,100);

		}
		if (FlxG.overlap(player, pickup_maskowl))
		{
			player.OwlMask.hasMask = true;
			soundPickup_Owl.play();

			if(owlBubble.alive){
				owlBubble.x = player.x;
				owlBubble.y = player.y - 140;

				owlBubble.alpha = 1;
				owlBubble.animation.play("bubble");

				var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
				t.start(4.0,function(t:flixel.util.FlxTimer){
					owlBubble.alpha = 0;
					owlBubble.alive = false;
				});

				pickup_maskowl.color = FlxColor.fromRGB(100,100,100);

			}


		}
		if (FlxG.overlap(player, pickup_goat))
		{
			player.canMove = false;
			player.canInput = false;
			
			
			//player.sprite.animation.play("kick", true);
			player.doKick();
			player.update(FlxG.elapsed);
			
			var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
			t.start(1,function(t:flixel.util.FlxTimer){
				EndGame(true);
			});


			
		}
	}
	
	override public function draw ()
	{
		super.draw();
		
		if (player.LynxMask.isMaskActive)
		{
			lynxVision.draw();
		}
		else
		{
			blackScreenOnDark.draw();
		}
		
		player.drawHud();

		bearBubble.draw();
		deerBubble.draw();
		lynxBubble.draw();
		owlBubble.draw();
		goatBubble.draw();

		overlay.draw();
		//laddercollidegroup.draw();
	}
	
	function EndGame(b:Bool = false) 
	{
		if (!ending)
		{
			ending = true;
			FlxTween.tween(overlay, {alpha : 1.0}, 0.9);
			var t : FlxTimer = new FlxTimer();
			if (b == false)
			{
				t.start(1.0, function(t:FlxTimer): Void { FlxG.switchState(new MenuState()); } );
			}
			else
			{
				t.start(1.0, function(t:FlxTimer): Void { FlxG.switchState(new CreditsState()); } );
			}
		}
	}
	
	function checkDarkOverlay():Void 
	{
		var tx : Int = Std.int(player.x / 86);
		var ty : Int = Std.int(player.y / 86);
		
		
		player.canChangeBack2Normal = checkTileFree(tx, ty -1) && checkTileFree(tx - 1, ty -1) && checkTileFree(tx - 2, ty -1);
		
		if (backgroundLayer.getTile(tx, ty) == 124)
		{
			if (blackScreenOnDark.alpha <= 0.9)
					blackScreenOnDark.alpha += GameProperties.DarknessGrowspeed * FlxG.elapsed;
			if (lynxVision.alpha <= 0.9)
					lynxVision.alpha += GameProperties.DarknessGrowspeed * FlxG.elapsed;
		}
		else 
		{
			var v : Float = blackScreenOnDark.alpha;
			
			
			if (v > 0.0)
				v -= GameProperties.DarknessGrowspeed * FlxG.elapsed;
			if (v <= 0.0)
				v = 0.0;
			
			lynxVision.alpha = blackScreenOnDark.alpha = v;
				
		}
	}
	
	public function checkPlayerCollide()
	{
		return !FlxG.overlap(player, level);
		
	}
	
	public function checkTileFree (x : Int, y : Int)
	{
		var i : Int = level.getTile(x, y);
		//trace(i);
		return level.getTile(x, y) == 0;
	}

	public function gameStart(){

		player.canMove = false;
		player.canInput = false;

		goatAssAnim.animation.play("ass");
		

		var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
		t.start(0.5,function(t:flixel.util.FlxTimer){
			goatAssAnim.x += 20;
		});
		var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
		t.start(0.5,function(t:flixel.util.FlxTimer){
			goatAssAnim.x += 20;
		});

		var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
		t.start(1.0,function(t:flixel.util.FlxTimer){
			goatBubble.alpha = 1;
			goatBubble.animation.play("bubble");

			goatAssAnim.alpha = 0;
			

		});

		var t : flixel.util.FlxTimer = new flixel.util.FlxTimer();
		t.start(4.0,function(t:flixel.util.FlxTimer){
			goatBubble.alpha = 0;
			player.canMove = true;
			player.canInput = true;
			grandmaright.alpha = 0;
			player.sprite.alpha = 1;

		});



	}
	
}
