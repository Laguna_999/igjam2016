package;
import flixel.math.FlxRandom;

/**
 * ...
 * @author 
 */
class GameProperties
{
	static public var Player_Movement_JumpStrength : Float = 375;
	static public var Player_Movement_WalkAcceleration : Float = 1125;
	static public var Player_Movement_AirControl_AccelFactor : Float = 2;
	static public var Player_Movement_MaxVelocityX : Float = 175;
	static public var Player_Movement_MaxVelocityY : Float = 500;
	static public var World_Physics_Gravity : Float = 800;
	static public var rng : FlxRandom = new FlxRandom();
	static public var Player_identityLossPerSecondPerMask : Float = 0.005;
	static public var Player_ladderClimbingSpeed  : Float  = 200;
	static public var Player_TakeDamageTimer : Float = 0.5;
	static public var DarknessGrowspeed : Float = 0.45;
	
	static public var Masks_EnergyDrain_Squirrel : Float = 1.15;
	static public var Masks_EnergyDrain_Bear : Float = 1.5;
	static public var Masks_EnergyDrain_Deer : Float = 1.5;
	static public var Masks_EnergyDrain_Owl : Float = 0.5;
	static public var Masks_EnergyDrain_Lynx : Float = 0.25;
}