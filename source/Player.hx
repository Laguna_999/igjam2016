package;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.group.FlxGroup;
import flixel.system.FlxAssets.FlxGraphicAsset;
import flixel.system.FlxSound;
import flixel.util.FlxColor;

/**
 * ...
 * @author 
 */
class Player extends FlxSprite
{
	/////////////////////////////
	// Movement Stuff
	/////////////////////////////
	private var touchesFloor : Bool = false;
	public var isOnGround : Bool = false;
	
	public  var touchedGround:Bool;
	public  var touchedGroundLast : Bool;
	private var touchedGroundTimer : Float;
	
	public var canMove : Bool;
	public var canInput : Bool;
	
	public var sprite : FlxSprite;
	
	/////////////////////////////
	// Mask Stuff
	/////////////////////////////
	
	public var DeerMask : Mask;
	public var BearMask : Mask;
	public var LynxMask : Mask;
	public var SquirrelMask : Mask;
	public var OwlMask : Mask;
	
	private var masklist : FlxTypedGroup<Mask>;
	
	public var isStrong : Bool;
	public var isSeeingInTheDark : Bool;
	
	public var identityBar : HudBar;
	public var isonLadder : Bool;
	public var canChangeBack2Normal : Bool = true;
	private var numberOfMasksActive : Int;
	private var takedamagetimer : Float;
	
	
	var heart : FlxSprite;
	var heartlifetime : Float  = 0;
	
	/////////////////////////////
	// Sounds
	/////////////////////////////
	private var stepsound1 : FlxSound;
	private var stepsound2 : FlxSound;
	private var stepsound3 : FlxSound;
	private var stepsound4 : FlxSound;
	private var jumpsound : FlxSound;
	private var walkSoundTimer : Float;
	private var owlFlysound : FlxSound;

	//Mask Sprites
	private var bearMaskSprite : FlxSprite;
	private var deerMaskSprite : FlxSprite;
	private var owlMaskSprite : FlxSprite;
	private var lynxMaskSprite : FlxSprite;
	private var squirrelMaskSprite : FlxSprite;

	private var bearMaskScaled : Bool = false;
	private var deerMaskScaled : Bool = false;
	private var owlMaskScaled : Bool = false;
	private var lynxMaskScaled : Bool = false;


	
	private var _state : PlayState;
	var F:Bool = false;
	var kicking : Bool = false;

	/////////////////////////////
	// Function
	/////////////////////////////
	public function new(?X:Float=0, ?Y:Float=0, state : PlayState) 
	{
		super(X, Y);
		_state = state;
		this.makeGraphic(Std.int(86 * 5 / 7), 110, FlxColor.RED);
		this.origin.set(this.width / 2, this.height);
		this.scale.set(1.0, 1.0); 
		
		sprite = new FlxSprite();
		sprite.loadGraphic(AssetPaths.playerAnim__png, true, 86, 140);
		sprite.origin.set(this.width / 2, this.height);
		sprite.scale.set(1.0, 1.0); 


		var idleIDs = [for (i in 0...32) i];
		var jumpIDs = [for (j in 33...64) j];
		var walkIDs = [for (k in 65...96) k];
		var kickIDs = [for (l in 97...112) l];
		
		sprite.animation.add("idle", idleIDs, 32, true);
		sprite.animation.add("jump", jumpIDs, 32, true);
		sprite.animation.add("walk", walkIDs, 32, true);
		sprite.animation.add("kick", kickIDs, 16, true);
		sprite.animation.play("idle");

		//Initialize masks animation
		bearMaskSprite = new FlxSprite();
		bearMaskSprite.loadGraphic(AssetPaths.playerAnim_bear__png, true, 86, 140);
		bearMaskSprite.animation.add("idle", walkIDs, 32, true);
		bearMaskSprite.animation.add("jump", jumpIDs, 32, true);
		bearMaskSprite.animation.add("walk", idleIDs, 32, true);
		bearMaskSprite.animation.play("idle");

		deerMaskSprite = new FlxSprite();
		deerMaskSprite.loadGraphic(AssetPaths.playerAnim_deer__png, true, 86, 167);
		deerMaskSprite.animation.add("idle", idleIDs, 32, true);
		deerMaskSprite.animation.add("jump", jumpIDs, 32, true);
		deerMaskSprite.animation.add("walk", walkIDs, 32, true);
		deerMaskSprite.animation.play("idle");

		owlMaskSprite = new FlxSprite();
		owlMaskSprite.loadGraphic(AssetPaths.playerAnim_owl__png, true, 86, 140);
		owlMaskSprite.animation.add("idle", idleIDs, 32, true);
		owlMaskSprite.animation.add("jump", jumpIDs, 32, true);
		owlMaskSprite.animation.add("walk", walkIDs, 32, true);
		owlMaskSprite.animation.play("idle");

		lynxMaskSprite = new FlxSprite();
		lynxMaskSprite.loadGraphic(AssetPaths.playerAnim_lynx__png, true, 86, 152);
		lynxMaskSprite.animation.add("idle", idleIDs, 32, true);
		lynxMaskSprite.animation.add("jump", jumpIDs, 32, true);
		lynxMaskSprite.animation.add("walk", walkIDs, 32, true);
		lynxMaskSprite.animation.play("idle");

		squirrelMaskSprite = new FlxSprite();
		squirrelMaskSprite.loadGraphic(AssetPaths.playerAnim_squirrel__png, true, 140, 140);
		squirrelMaskSprite.animation.add("idle", idleIDs, 32, true);
		squirrelMaskSprite.animation.add("jump", jumpIDs, 32, true);
		squirrelMaskSprite.animation.add("walk", walkIDs, 32, true);
		squirrelMaskSprite.animation.play("idle");
		squirrelMaskSprite.scale.set(0.5,0.5);
		
		canMove  = false;
		canInput  = false;
		touchedGround = false;
		touchedGroundTimer = 0;
		
		setFacingFlip(FlxObject.LEFT, false, false);
		setFacingFlip(FlxObject.RIGHT, true, false);

		sprite.setFacingFlip(FlxObject.LEFT, false, false);
		sprite.setFacingFlip(FlxObject.RIGHT, true, false);
		
		bearMaskSprite.setFacingFlip(FlxObject.LEFT, false, false);
		bearMaskSprite.setFacingFlip(FlxObject.RIGHT, true, false);

		deerMaskSprite.setFacingFlip(FlxObject.LEFT, false, false);
		deerMaskSprite.setFacingFlip(FlxObject.RIGHT, true, false);

		owlMaskSprite.setFacingFlip(FlxObject.LEFT, false, false);
		owlMaskSprite.setFacingFlip(FlxObject.RIGHT, true, false);

		lynxMaskSprite.setFacingFlip(FlxObject.LEFT, false, false);
		lynxMaskSprite.setFacingFlip(FlxObject.RIGHT, true, false);

		squirrelMaskSprite.setFacingFlip(FlxObject.LEFT, false, false);
		squirrelMaskSprite.setFacingFlip(FlxObject.RIGHT, true, false);

		facing = FlxObject.RIGHT;
		//update(0.0001);

		this.acceleration.set(0, GameProperties.World_Physics_Gravity);
		this.maxVelocity.set(GameProperties.Player_Movement_MaxVelocityX, GameProperties.Player_Movement_MaxVelocityY);
		
		this.drag.set(2500, 100);
		
		
		
		
		/////////////////////////////
		// Sound Stuff
		/////////////////////////////
		stepsound1 = FlxG.sound.load(AssetPaths.T13_fs_dirt_1__ogg, 0.125);
		stepsound2 = FlxG.sound.load(AssetPaths.T13_fs_dirt_2__ogg, 0.125);
		stepsound3 = FlxG.sound.load(AssetPaths.T13_fs_dirt_3__ogg, 0.125);
		stepsound4 = FlxG.sound.load(AssetPaths.T13_fs_dirt_4__ogg, 0.125);
		jumpsound = FlxG.sound.load(AssetPaths.T13_jump__ogg, 1.5);
		owlFlysound = FlxG.sound.load(AssetPaths.T13_bird_flying__ogg, 1.0, true);
		owlFlysound.volume = 0;
		owlFlysound.play();
		walkSoundTimer = 0;
		
		/////////////////////////////
		// Mask Stuff
		/////////////////////////////
		
		identityBar = new HudBar(128, 128*0.75+10,  FlxG.width - 192 , 24, false);
		identityBar.scrollFactor.set();
		
		heart = new FlxSprite( 10, 10);
		heart.loadGraphic(AssetPaths.heart__png, false, 108, 126);
		heart.scrollFactor.set();
		
		numberOfMasksActive = 0;
		takedamagetimer = 0;
		
		SquirrelMask = new Mask(AssetPaths.ui_mask_squirrel__png, AssetPaths.ui_mask_squirrel_active__png, 1, GameProperties.Masks_EnergyDrain_Squirrel);
		BearMask = new Mask(AssetPaths.UI_mask_bear__png, AssetPaths.UI_mask_bear_active__png, 2, GameProperties.Masks_EnergyDrain_Bear);
		DeerMask = new Mask(AssetPaths.UI_mask_deer__png, AssetPaths.UI_mask_deer_active__png, 3, GameProperties.Masks_EnergyDrain_Deer);
		OwlMask = new Mask(AssetPaths.ui_mask_owl__png, AssetPaths.ui_mask_owl_active__png, 4, GameProperties.Masks_EnergyDrain_Owl);
		LynxMask = new Mask(AssetPaths.ui_mask_lynx__png, AssetPaths.ui_mask_lynx_active__png, 5, GameProperties.Masks_EnergyDrain_Lynx );
		isStrong = false;
		isSeeingInTheDark = false;
		
		DeerMask.hasMask = false;
		DeerMask.isMaskActive = false;
		DeerMask.isHead = true;
		DeerMask.isFace = false;
		
		BearMask.hasMask = false;
		BearMask.isMaskActive = false;
		BearMask.isHead = false;
		BearMask.isFace = true;
		
		LynxMask.hasMask = false;
		LynxMask.isMaskActive = false;
		LynxMask.isHead = true;
		LynxMask.isFace = false;
		
		SquirrelMask.hasMask = true;
		SquirrelMask.isMaskActive = false;
		SquirrelMask.isHead = false;
		SquirrelMask.isFace = false;
		
		OwlMask.hasMask = false;
		OwlMask.isMaskActive = false;
		OwlMask.isHead = false;
		OwlMask.isFace = true;
		
		masklist = new FlxTypedGroup<Mask>();
		masklist.add(DeerMask);
		masklist.add(BearMask);
		masklist.add(LynxMask);
		masklist.add(SquirrelMask);
		masklist.add(OwlMask);
	}
	
	public override function update (elapsed : Float)
	{
		if (kicking)
		{
			super.update(elapsed);
			
			return;
		}
		
		
		
		//trace(DeerMask.isMaskActive);
		isOnGround = (velocity.y == 0);
		takedamagetimer -= elapsed;
		this.acceleration.x = 0;
		sprite.setPosition(x-86/7, y-20);
		
		bearMaskSprite.x = x-86/7;
		bearMaskSprite.y = y-20;

		deerMaskSprite.x = x-86/7;
		deerMaskSprite.y = y - 27-20;

		owlMaskSprite.x = x-86/7;
		owlMaskSprite.y = y-20;

		lynxMaskSprite.x = x-86/7;
		lynxMaskSprite.y = y - 12-20;

		//squirrelMaskSprite.x = x - 60;
		squirrelMaskSprite.y = y - 40-20;

		if(SquirrelMask.isMaskActive){
			//check if other Masks are active, if so then scale it.
			if(BearMask.isMaskActive){
				bearMaskSprite.scale.set(0.5,0.5);
				bearMaskScaled = true;
				bearMaskSprite.x = x-35;
				bearMaskSprite.y = y-55;
			}
			if(OwlMask.isMaskActive){
				owlMaskSprite.scale.set(0.5,0.5);
				owlMaskScaled = true;
				owlMaskSprite.x = x-35;
				owlMaskSprite.y = y-55;
			}
			if(DeerMask.isMaskActive){
				deerMaskSprite.scale.set(0.5,0.5);
				deerMaskScaled = true;
				deerMaskSprite.x = x-35;
				deerMaskSprite.y = y - 27-50;
			}
			if(LynxMask.isMaskActive){
				lynxMaskSprite.scale.set(0.5,0.5);
				lynxMaskScaled = true;
				lynxMaskSprite.x = x-35;
				lynxMaskSprite.y = y - 12 -55;
			}
		}else{
			if(bearMaskScaled){
				bearMaskSprite.scale.set(1,1);
				bearMaskScaled = false;
			}
			if(owlMaskScaled){
				owlMaskSprite.scale.set(1,1);
				owlMaskScaled = false;
			}
			if(deerMaskScaled){
				deerMaskSprite.scale.set(1,1);
				deerMaskScaled = false;
			}
			if(lynxMaskScaled){
				lynxMaskSprite.scale.set(1,1);
				lynxMaskScaled = false;
			}

		}

		
		if (touchedGround == true) 
		{
			touchedGroundTimer = 0;
		}
		else
		{
			touchedGroundTimer += elapsed;
			walkSoundTimer = 0;
		}
		var energydrain : Float = 0;
		for (i in 0...masklist.length)
		{
			var m : Mask  = masklist.members[i];
			if (m.isMaskActive) energydrain += m.energydrainfactor;
		}
		
		identityBar.health -= elapsed * GameProperties.Player_identityLossPerSecondPerMask * energydrain;
		identityBar.update(elapsed);
		
		if (canInput )
		{
			var vx = MyInput.xVal;
			var ac : Float = GameProperties.Player_Movement_WalkAcceleration * (isOnGround ? 1 : GameProperties.Player_Movement_AirControl_AccelFactor);
			this.acceleration.x = vx * ac;
			
			isStrong = BearMask.isMaskActive;
			isSeeingInTheDark = LynxMask.isMaskActive;
			
			
			if (vx > 0)
			{
				facing = FlxObject.RIGHT;
				sprite.facing = FlxObject.RIGHT;
				bearMaskSprite.facing = FlxObject.RIGHT;
				deerMaskSprite.facing = FlxObject.RIGHT;
				owlMaskSprite.facing = FlxObject.RIGHT;
				lynxMaskSprite.facing = FlxObject.RIGHT;
				squirrelMaskSprite.facing = FlxObject.RIGHT;
				squirrelMaskSprite.x = x - 60-86/7;
			}
			else if (vx < 0)
			{
				facing = FlxObject.LEFT;
				sprite.facing = FlxObject.LEFT;
				bearMaskSprite.facing = FlxObject.LEFT;
				deerMaskSprite.facing = FlxObject.LEFT;
				owlMaskSprite.facing = FlxObject.LEFT;
				lynxMaskSprite.facing = FlxObject.LEFT;
				squirrelMaskSprite.facing = FlxObject.LEFT;
				squirrelMaskSprite.x = x - 35-86/7;
			}
			if (Math.abs(velocity.x ) > 0.5 && touchedGround)
			{

				walkSoundTimer -= elapsed;
				if (walkSoundTimer <= 0)
				{
					var i : Int = GameProperties.rng.int(0, 3);
					if (i == 0) stepsound1.play();
					if (i == 1) stepsound2.play();
					if (i == 2) stepsound3.play();
					if (i == 3) stepsound4.play();
					walkSoundTimer += 0.6;
				}
			}
			else
			{
			}
			
			if (!isonLadder)
			{
			if (MyInput.JumpButtonJustPressed && (isOnGround || touchedGroundTimer < 0.2 ) )
			{
				jumpsound.play();
				stepsound1.play();
				stepsound3.play();
				var jumpfactor = DeerMask.isMaskActive? 5 : 1;
				
				walkSoundTimer = 0;
				y -= 1;
				velocity.y = -GameProperties.Player_Movement_JumpStrength * jumpfactor;
				sprite.animation.play("jump", true);
				bearMaskSprite.animation.play("jump",true);
				deerMaskSprite.animation.play("jump",true);
				squirrelMaskSprite.animation.play("jump",true);
				owlMaskSprite.animation.play("jump",true);
				lynxMaskSprite.animation.play("jump",true);
				//jumpSound.play();
			}
			
			if (velocity.y > 0)	// going down
			{
				if (OwlMask.isMaskActive)
				{
					this.drag.set(2500, 2500);
					this.acceleration.set(this.acceleration.x, GameProperties.World_Physics_Gravity / 8);
					this.maxVelocity.set(this.maxVelocity.x, GameProperties.Player_Movement_MaxVelocityY / 3);
					owlFlysound.volume = 1.0;
				}
				else
				{
					this.acceleration.set(this.acceleration.x, GameProperties.World_Physics_Gravity);
					this.maxVelocity.set(this.maxVelocity.x, GameProperties.Player_Movement_MaxVelocityY);
					this.drag.set(2500, 100);
					owlFlysound.volume = 0;
				}
				
			}
			else 	// going up or standing still
			{
				this.drag.set(2500, 100);
				this.acceleration.set(this.acceleration.x, GameProperties.World_Physics_Gravity);
				this.maxVelocity.set(this.maxVelocity.x, GameProperties.Player_Movement_MaxVelocityY);
				owlFlysound.volume = 0;
			}
			}
			else
			{
				// ladder
				this.velocity.y = 0;
				this.acceleration.y = 0;
				var jumpfactor = DeerMask.isMaskActive? 4.5 : 1;
				if (MyInput.JumpButtonPressed )
				{	
					this.velocity.y = -GameProperties.Player_ladderClimbingSpeed * jumpfactor;
				}
				else if (MyInput.DownButtonPressed )
				{	
					this.velocity.y =  GameProperties.Player_ladderClimbingSpeed;
				}
				
			}
			
			CheckMaskInput();
			
			
		}
		
		
		if (velocity.y == 0)
		{
			if (velocity.x > 0)
			{
				sprite.animation.play("walk");
				bearMaskSprite.animation.play("walk");
				deerMaskSprite.animation.play("walk");
				owlMaskSprite.animation.play("walk");
				lynxMaskSprite.animation.play("walk");
				squirrelMaskSprite.animation.play("walk");
			}
			else if (velocity.x < 0)
			{
				sprite.animation.play("walk");
				bearMaskSprite.animation.play("walk");
				deerMaskSprite.animation.play("walk");
				owlMaskSprite.animation.play("walk");
				lynxMaskSprite.animation.play("walk");
				squirrelMaskSprite.animation.play("walk");
			}
			else
			{
				sprite.animation.play("idle");
				bearMaskSprite.animation.play("idle");
				deerMaskSprite.animation.play("idle");
				owlMaskSprite.animation.play("idle");
				lynxMaskSprite.animation.play("idle");
				squirrelMaskSprite.animation.play("idle");
			}
		}
	
	

		
		if (canMove)
		{
			
			var freq = 3;
			
			heartlifetime += elapsed * ( 4 - identityBar.health * 3 );
			

			
			var v : Float = Math.cos(Math.sin(heartlifetime * freq) + heartlifetime * freq);
			if (v < 0) 
			{
				v = -v;
			}
			v = 0.75 + v * 0.25;
			heart.scale.set(v, v);
			
			super.update(elapsed);
			sprite.update(elapsed);
			bearMaskSprite.update(elapsed);
			deerMaskSprite.update(elapsed);
			owlMaskSprite.update(elapsed);
			lynxMaskSprite.update(elapsed);
			squirrelMaskSprite.update(elapsed);

			if (touchedGround == true && touchedGroundLast == false)
			{
				// spawn particles
			}
			
		}
	}
	
	function CheckMaskInput() 
	{
		if (FlxG.keys.justPressed.ONE)
		{
			
			if (SquirrelMask.hasMask)
			{
				if (!SquirrelMask.isMaskActive)
				{	// now it is active
					SquirrelMask.isMaskActive = true;
					//this.origin.set(this.width / 2, this.width / 2);
					this.y += 30;
					sprite.y += 30;
					
					this.scale.set(0.5, 0.5);
					sprite.scale.set(0.5, 0.5);
					
					this.updateHitbox();
					sprite.updateHitbox();
				}
				else
				{
					if (canChangeBack2Normal)
					{
					
						SquirrelMask.isMaskActive = false;
						this.scale.set(1.0, 1.0);
						sprite.scale.set(1.0, 1.0);
						this.y -= 69;
						sprite.y -= 69;
						
						
						this.updateHitbox();
						sprite.updateHitbox();
					}	
				}
			}

		}
		else if (FlxG.keys.justPressed.TWO)
		{
			if (BearMask.hasMask)
			{
				if (!BearMask.isMaskActive)
				{
					BearMask.isMaskActive = true;
					OwlMask.isMaskActive = false;
				}
				else
				{
					BearMask.isMaskActive = false;
				}
			}
		
		}
		else if (FlxG.keys.justPressed.THREE)
		{
				if (DeerMask.hasMask)
			{
				if (!DeerMask.isMaskActive)
				{
					DeerMask.isMaskActive = true;
					LynxMask.isMaskActive = false;
					
				}
				else
				{
					DeerMask.isMaskActive = false;
					
				}
			}
		}
		else if (FlxG.keys.justPressed.FOUR)
		{
			if (OwlMask.hasMask)
			{
				if (!OwlMask.isMaskActive)
				{
					trace("owl now active");
					OwlMask.isMaskActive = true;
					BearMask.isMaskActive = false;
				}
				else
				{
					trace("owl now deactive");
					OwlMask.isMaskActive = false;
				}
			}
		}
		else if (FlxG.keys.justPressed.FIVE)
		{
				if (LynxMask.hasMask)
			{
				if (!LynxMask.isMaskActive)
				{
					LynxMask.isMaskActive = true;
					DeerMask.isMaskActive = false;
				}
				else
				{
					LynxMask.isMaskActive = false;
				}
			}
		}
		else if (FlxG.keys.justPressed.ZERO)
		{
			if (SquirrelMask.isMaskActive)
			{
				SquirrelMask.isMaskActive = false;
				this.scale.set(1.0, 1.0);
				sprite.scale.set(1.0, 1.0);
				this.updateHitbox();
			}
		}
	}
	
	public override function draw () 
	{
		sprite.draw();
		//super.draw();
		
		if(BearMask.isMaskActive)
			bearMaskSprite.draw();
		if(DeerMask.isMaskActive)
			deerMaskSprite.draw();
		if(SquirrelMask.isMaskActive)
			squirrelMaskSprite.draw();
		if(OwlMask.isMaskActive)
			owlMaskSprite.draw();
		if(LynxMask.isMaskActive)
			lynxMaskSprite.draw();
	}
	
	public function drawHud()
	{
		identityBar.draw();
		heart.draw();
		masklist.draw();
	}
	
	public function refillIdentity() 
	{
		identityBar.health += 0.25 * FlxG.elapsed;
		if (identityBar.health > 1) identityBar.health = 1;
	}
	
	public function TakeDamage() 
	{
		if (takedamagetimer <= 0)
		{
			takedamagetimer = GameProperties.Player_TakeDamageTimer;
			identityBar.health -= 0.075;
		}
	}
	
	public function doKick()
	{
		if (!kicking)
		{
			kicking = true;
			sprite.animation.play("kick", true);
		}
	}
	
}