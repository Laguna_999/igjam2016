package;
import flixel.FlxG;
import flixel.input.gamepad.FlxGamepad;
import flixel.input.gamepad.FlxGamepadInputID;

/**
 * ...
 * @author 
 */
class MyInput
{
	public static var xVal : Float = 0;
	public static var JumpButtonJustPressed : Bool;
	public static var JumpButtonPressed : Bool;
	public static var DownButtonPressed : Bool;
	public static var AttackButton : Bool;
	
	public static function update ()
	{
		JumpButtonJustPressed = false;
		JumpButtonPressed = false;
		DownButtonPressed = false;
		AttackButton = false;
		
		xVal = 0;
		var gp : FlxGamepad = FlxG.gamepads.firstActive;
		if (gp != null)
		{
			xVal = gp.getXAxis(FlxGamepadInputID.LEFT_ANALOG_STICK);
			JumpButtonJustPressed = gp.justPressed.A;
			AttackButton = gp.justPressed.X;
		}
		
		if (FlxG.keys.pressed.D || FlxG.keys.pressed.RIGHT)
		{
			//trace("right");
			xVal = 1;
		}
		if (FlxG.keys.pressed.A || FlxG.keys.pressed.LEFT)
		{
			//trace("left");
			xVal = -1;
		}
		
		if (FlxG.keys.pressed.UP || FlxG.keys.pressed.W )
		{
			JumpButtonPressed = true;
		}
		if (FlxG.keys.justPressed.UP ||FlxG.keys.justPressed.W ||FlxG.keys.justPressed.SPACE)
		{
			JumpButtonJustPressed = true;
		}
		if (FlxG.keys.justPressed.C || FlxG.keys.justPressed.CONTROL)
		{
			AttackButton = true;
		}
		if (FlxG.keys.pressed.DOWN || FlxG.keys.pressed.S)
		{
			DownButtonPressed = true;
		}
	}
	
}