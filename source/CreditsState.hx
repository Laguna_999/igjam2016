package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.tweens.FlxTween;
import flixel.util.FlxAxes;
import flixel.util.FlxCollision;
import flixel.util.FlxColor;

/**
 * ...
 * @author 
 */
class CreditsState extends FlxState
{
	private var background : FlxSprite;
	var text : FlxText;
	var timer : Float = 0;
	
	private var title : FlxText;
	
	override public function create():Void
	{
		
		background = new FlxSprite();
		background.scrollFactor.set();
		background.makeGraphic(FlxG.width, FlxG.height, FlxColor.fromRGB(230, 230, 230));
		add(background);
		
		
		title = new FlxText(0, 0, 0, "Koza  i  Babushka", 64);
		title.setFormat(AssetPaths.WereWolf__ttf, 64, FlxColor.BLACK);
		title.alignment = flixel.text.FlxTextAlign.CENTER;
		title.screenCenter(FlxAxes.X);
		title.y = FlxG.height / 3;
		title.scrollFactor.set();
		add(title);
		
		text = new FlxText(100, 0, 0, "", 26);
		text.text = "A game by\n\n" +
		"Nathalie  Michel\n-graphics-\n\nPatrick  Aartssen \n-level  design-" + 
		"\n\nDustin  Leibnitz \n-animations-\n\nSimon  Weis \n-code-\n\n" +
		"and\nthe  awesome  sound  team \n -sounds  and  music-\n\n\n" +
		"created  for  IGJam16  at  Gamescom  Cologne\n2016-08-17  to  2016-08-19\n\n" +
		"Tools  used\n HaxeFlixel, Ogmo, Spriter Pro, \nAutodesk Sketch, Paint.NET,\nAffinity Photo, SmartTimelapse,\nspeedcrunch, cygwin, notepadpp" +
		"\nFont and sounds not created by the core team"	;
		text.alignment = flixel.text.FlxTextAlign.CENTER;
		text.setFormat(AssetPaths.WereWolf__ttf, 45, FlxColor.BLACK);
		text.screenCenter(FlxAxes.X);
		add(text);
		text.alpha = 0;
		FlxTween.tween(text, { alpha:1 }, 0.5);
	}
	
	public override function update (elapsed : Float) : Void
	{
		super.update(elapsed);
		
		var vel : Float = 40;
		text.y = 450 - vel * timer;
		
		title.y =  FlxG.height / 3 - vel * timer;
		
		
		timer += elapsed;
		if (timer >= 0.5)
		{
			if (FlxG.keys.pressed.ANY)
			{
				FlxG.switchState(new MenuState());
			}
		}
		if (timer >= 40)
		{
			FlxG.switchState(new MenuState());
		}
	}

	
}